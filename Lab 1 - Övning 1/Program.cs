﻿using System; // Error CS0103  The name 'Console' does not exist in the current context.


namespace Simple
{
    class Program // Error	CS0246	The type or namespace name 'klass' could not be found (are you missing a using directive or an assembly reference?) + lite andra fel.
    {
        static void Main() // Error Program does not contain a static 'Main' method suitable for an entry point.
        {
            int numberInt = 123;            // Skapat en intiger för att testa med.
            double numberDouble = 123.123;  // Skapat en double för att se skilnaden med ett decimaltal.

            // Skapat en linje med var nummerformaterare och testat standard värdet och även om man sätter länden/precitonen till 4 så man kan se skilnaden.
            Console.WriteLine($"N2 Left  |{numberInt,-15:N2}|"); 
            Console.WriteLine($"N2 Right |{numberInt,15:N2}|");
            
            Console.WriteLine($"");
            Console.WriteLine($"-------------int-----------------------");
            Console.WriteLine($"");

            Console.WriteLine("R  |{0,15:r}|","10");
            Console.WriteLine($"C  |{numberInt,15:C}|");
            Console.WriteLine($"D  |{numberInt,15:D}|");
            Console.WriteLine($"F  |{numberInt,15:F}|");
            Console.WriteLine($"G  |{numberInt,15:G}|");
            Console.WriteLine($"X  |{numberInt,15:X}|");
            Console.WriteLine($"x  |{numberInt,15:x}|");
            Console.WriteLine($"N  |{numberInt,15:N}|");
            Console.WriteLine($"P  |{numberInt,15:P}|");
            Console.WriteLine($"E  |{numberInt,15:E}|");
            Console.WriteLine($"e  |{numberInt,15:e}|");

            Console.WriteLine($"");

            Console.WriteLine("R4 |{0,15:r}|", "10");
            Console.WriteLine($"C4 |{numberInt,15:C4}|");
            Console.WriteLine($"D4 |{numberInt,15:D4}|");
            Console.WriteLine($"F4 |{numberInt,15:F4}|");
            Console.WriteLine($"G4 |{numberInt,15:G4}|");
            Console.WriteLine($"X4 |{numberInt,15:X4}|");
            Console.WriteLine($"x4 |{numberInt,15:x4}|");
            Console.WriteLine($"N4 |{numberInt,15:N4}|");
            Console.WriteLine($"P4 |{numberInt,15:P4}|");
            Console.WriteLine($"E4 |{numberInt,15:E4}|");
            Console.WriteLine($"e4 |{numberInt,15:e4}|");

            Console.WriteLine($"");
            Console.WriteLine($"-------------double--------------------");
            Console.WriteLine($"");

            Console.WriteLine($"C  |{numberDouble,15:C}|");
            //Console.WriteLine($"D  |{numberDouble,15:D}|"); // Fungerar ändast med int (heltal).
            Console.WriteLine($"F  |{numberDouble,15:F}|");
            Console.WriteLine($"G  |{numberDouble,15:G}|");
            //Console.WriteLine($"X  |{numberDouble,15:X}|"); // Fungerar inte på duble då det görs om till hexadecimal.
            //Console.WriteLine($"x  |{numberDouble,15:x}|");
            Console.WriteLine($"N  |{numberDouble,15:N}|");
            Console.WriteLine($"P  |{numberDouble,15:P}|");
            Console.WriteLine($"E  |{numberDouble,15:E}|");
            Console.WriteLine($"e  |{numberDouble,15:e}|");
                                     
            Console.WriteLine($"");  
                                    
            Console.WriteLine($"C4 |{numberDouble,15:C4}|");
            //Console.WriteLine($"D4 |{numberDouble,15:D4}|");
            Console.WriteLine($"F4 |{numberDouble,15:F4}|");
            Console.WriteLine($"G4 |{numberDouble,15:G4}|");
            //Console.WriteLine($"X4 |{numberDouble,15:X4}|");
            //Console.WriteLine($"x4 |{numberDouble,15:x4}|");
            Console.WriteLine($"N4 |{numberDouble,15:N4}|");
            Console.WriteLine($"P4 |{numberDouble,15:P4}|");
            Console.WriteLine($"E4 |{numberDouble,15:E4}|");
            Console.WriteLine($"e4 |{numberDouble,15:e4}|");

            //Console.ReadKey(); // Error CS1002 ; expected.
        } // Error CS1513  }.
    }
}
