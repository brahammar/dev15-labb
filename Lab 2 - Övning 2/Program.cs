﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2___Övning_2
{
    class Program
    {
        private static double pi;

        static void Main(string[] args)
        {
            Exchange();

            Pie();

            HeLives();
        }

        private static void HeLives()
        {
            //Han heter Jonas och bor i Helsingborg på Stattena vid Hemköp.

            string name = "Jonas";
            string place = "Helsingborg";

            Console.Write($"Han heter {name} och bor i {place} ");

            place = "Stattena";
            Console.Write($"på {place} ");

            place = "Hemköp";
            Console.Write($"vid {place}.");

            Console.WriteLine();
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine();
        }

        private static void Pie()
        {
            //Kuppfråga.
            const double PI = 3.141592654;
            //PI = 3.14; // Kompileringsfel.
            Console.WriteLine($"Talet π är cirka {PI} men kan avrundas till {PI:N2} och kan lite grovt skrivas som {PI:N0}.");

            Console.WriteLine();
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine();
        }

        private static void Exchange()
        {
            string name = "Dow Jones";
            double change = 4.6;

            Console.Write($"{name} sjönk {change} procent");

            name = "Nasdaq";
            change = 4.1;

            Console.Write($" och {name} med {change} procent. ");

            name = "Stockholm";
            change = -2.4;

            Console.Write($"{name} med {change} procent.");

            name = "Milano";
            change = -6.7;

            Console.Write($"{name} med {change} procent, ");

            name = "Paris";
            change = -5.5;

            Console.Write($"{name} med {change} procent.");

            Console.WriteLine();
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine();
        }
    }
}
