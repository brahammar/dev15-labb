﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3_Extra
{
    class Circle
    {
        public double Diameter { get; set; }
        public Point Point { get; set; }

        public Circle(double diameter, Point point)
        {
            this.Diameter = diameter;
            this.Point = point;
        }

        public Circle(double diameter, double x, double y) : this(diameter, new Point(x, y)) { }

        public void ChangeCircleToCrossOrigo()
        {
                this.Diameter = this.DistanceToOrigo() * 2;  
        }
        public double DistanceToOrigo()
        {
            return Math.Sqrt(Math.Pow(this.Point.X, 2) + Math.Pow(this.Point.Y, 2));
        }

        public Circle CreateMirroredCircle()
        {
            return new Circle(this.Diameter, this.Point.X * -1, this.Point.Y * -1);
        }

        public List<Circle> GenerateInternalCircles()
        {
            List<Circle> circleList = new List<Circle>();
            double diameterNewCircle = this.Diameter / 2;
            double newCircleCenterOffset = diameterNewCircle;
            double topLeftXCordinat = this.Point.X - this.Diameter / 4;
            double topLeftYCordinat = this.Point.Y + this.Diameter / 4;

            circleList.Add(new Circle(diameterNewCircle, topLeftXCordinat, topLeftYCordinat));
            circleList.Add(new Circle(diameterNewCircle, topLeftXCordinat +  newCircleCenterOffset, topLeftYCordinat));
            circleList.Add(new Circle(diameterNewCircle, topLeftXCordinat, topLeftYCordinat - newCircleCenterOffset));
            circleList.Add(new Circle(diameterNewCircle, topLeftXCordinat + newCircleCenterOffset, topLeftYCordinat - newCircleCenterOffset));

            return circleList;
        }

        public double GetArea() //Fråga användaren efter en area och skriv ut de Cirklar som har en Area som är större än den angivna.
        {
            return Math.PI * Math.Pow((this.Diameter / 2), 2);
        }

        public double GetCircumference()
        {
            return Math.PI * (this.Diameter / 2) * 2;
        }
        public void MoveHorizontaly(int changeX) //Fråga användren efter ett tal och flytta alla cirklarna så mycket som angetts i X-led.
        {
            this.Point.X += changeX;
        }

        public void MoveVerticaly(int changeY) //Fråga användren efter ett tal och flytta alla cirklarna så mycket som angetts i Y-led.
        {
            this.Point.Y += changeY;
        }
    }
}
