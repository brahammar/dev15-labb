﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3_Extra
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.StartTheCircle(1000);
        }

        private void StartTheCircle(int numberOfCircles)
        {
            Random random = new Random();
            var maxCircleDiameter = 100;
            var maxDiagramX = 200;
            var maxDiagramY = 200;

            List<Circle> circleList = new List<Circle>();

            Circle tempCircle;
            int tempCircleDiameter;
            int tempXCord;
            int tempYCord;

            for (int i = 0; i < numberOfCircles; i++)
            {
                tempCircleDiameter = random.Next(1, maxCircleDiameter);
                tempXCord = random.Next(maxDiagramX * -1, maxDiagramX);
                tempYCord = random.Next(maxDiagramY * -1, maxDiagramY);

                tempCircle = new Circle(tempCircleDiameter, tempXCord, tempYCord);
                circleList.Add(tempCircle);


                //Console.WriteLine($"{i, 3}: {tempCircle.GetCircumference(), 6:N2} | {tempCircle.GetArea(), 8:N2}");
            }

            #region Get number of circles buy diameter and buy circumference.
            int circlesWithDiameter = circleList.Count(circle => circle.Diameter >= 7 && circle.Diameter <= 17);
            int circlesWithCircumference = circleList.Count(circle => circle.GetCircumference() >= 2 && circle.GetCircumference() <= 22);
            Console.WriteLine($"Number of circles with diameter betven 7 och 17: {circlesWithDiameter}");
            Console.WriteLine($"Number of circles with circumference betven 2 och 22: {circlesWithCircumference}");
            #endregion

            #region Change circles to cross origo.
            List<Circle> circlesCloseToOrigo = circleList.Where(circle => circle.DistanceToOrigo() <= 12).ToList();

            for (int i = 0; i < circlesCloseToOrigo.Count; i++)
            {
                circlesCloseToOrigo[i].ChangeCircleToCrossOrigo();
            }
            #endregion
            
            //Console.ReadKey();
        }
    }
}