﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3
{
    class Circle
    {
        private double diameter;
        private Point center;

        public Point GetCenter
        {
            get
            {
                return this.center;
            }
        }
        public Point SetCenter
        {
            set
            {
                this.center = value;
            }
        }

        public void SetDiameter(double diameter)
        {
            this.diameter = diameter;
        }

        public double GetDiameter()
        {
            return this.diameter;
        }

        public double GetCircumference()
        {
            return Math.PI * diameter;
        }
    }
}
