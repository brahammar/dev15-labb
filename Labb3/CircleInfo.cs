﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3
{
    class CircleInfo
    {
        public int Diameter{get; private set;}
        public Point Coardinates {get; private set;}

        public CircleInfo(int diameter, Point cordinates)
        {
            this.Diameter = diameter;
            this.Coardinates = cordinates;
        }
    }
}
