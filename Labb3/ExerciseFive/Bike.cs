﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3.ExerciseFive
{
    class Bike : Vehicle
    {
        public const string VEHICLETYPE = "Bike";

        public Bike(Color color, Transmission transmission, int numberOfWheels)
        {
            this.Color = color;
            this.Transmission = transmission;
            this.NumberOfWheels = numberOfWheels;
        }
    }
}
