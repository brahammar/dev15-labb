﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3.ExerciseFive
{
    class Car : Vehicle
    {
        public const string VEHICLETYPE = "Car";
        public int HorsePower { get; private set; }

        public Car(Color color, Transmission transmission, int numberOfWheels, int horsePower)
        {
            this.Color = color;
            this.Transmission = transmission;
            this.NumberOfWheels = numberOfWheels;
            this.HorsePower = horsePower;
        }   
    }
}
