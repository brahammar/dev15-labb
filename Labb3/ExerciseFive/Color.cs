﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3.ExerciseFive
{
    class Color
    {
        public string Name { get; private set; }
        public bool IsMetalic { get; private set; }
        public Color(string name, bool isMetalic)
        {
            this.Name = name;
            this.IsMetalic = isMetalic;
        }
    }
}
