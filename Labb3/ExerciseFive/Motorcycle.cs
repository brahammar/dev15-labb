﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3.ExerciseFive
{
    class Motorcycle : Vehicle
    {
        public const string VEHICLETYPE = "Motorcycle";
        public int HoresPower { get; private set; }

        public Motorcycle(Color color, Transmission transmission, int numberOfWheels, int horsePower)
        {
            this.Color = color;
            this.Transmission = transmission;
            this.NumberOfWheels = numberOfWheels;
            this.HoresPower = horsePower;
        }
    }
}
