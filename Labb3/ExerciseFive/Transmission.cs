﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3.ExerciseFive
{
    class Transmission
    {
        private bool _isInternal;

        public bool IsInternal
        {
            get
            {
                return this._isInternal;
            }
            private set
            {
                this._isInternal = value;
            }
        }
        public int Geras{ get; private set; }

        public Transmission(bool isInternal, int numberOfGears)
        {
            this.IsInternal = isInternal;
            this.Geras = numberOfGears;
        }
    }
}
