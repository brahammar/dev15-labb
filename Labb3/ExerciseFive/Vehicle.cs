﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3.ExerciseFive
{
    class Vehicle
    {
        public Color Color { get; set; }
        public Transmission Transmission { get; protected set; }
        public int NumberOfWheels { get; protected set; }
    }
}
