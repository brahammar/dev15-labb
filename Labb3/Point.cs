﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3
{
    public class Point
    {
        public int Y { get; set; }

        public int X { get; set; }

        public Point()
        {
            
        }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public void SetX(int x)
        {
            this.X = x;
        }

        public int GetX()
        {
            return this.X;
        }

        public void SetY(int y)
        {
            this.X = y;
        }

        public int GetY()
        {
            return this.Y;
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }
}
