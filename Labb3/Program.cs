﻿using Labb3.ExerciseFive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb3
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();

            Console.WriteLine("-= People() =-");
            program.People();

            program.ConsoleSeparator();

            Console.WriteLine("-= Circle() =-");
            program.Circle();

            program.ConsoleSeparator();

            Console.WriteLine("-= Vehicle() =-");
            program.Vehicle();
        }

        private void Vehicle()
        {
            var bike = new Bike(
                    new Color("Red", true),
                    new Transmission(false, 21),
                    2
                );
            var mc = new Motorcycle(
                    color: new Color("Blue", false),
                    transmission: new Transmission(true, 4),
                    numberOfWheels: 2,
                    horsePower: 200
                );
            var car = new Car(new Color("Hotpink", true), new Transmission(true, 5), 4, 150);

            Console.WriteLine("Bike");
            Console.WriteLine("Color: {0}{1}", bike.Color.Name, bike.Color.IsMetalic ? " metalic" : "");
            Console.WriteLine("Geras: {0} speed {1}", bike.Transmission.Geras, bike.Transmission.IsInternal ? "gearbox" : "external");
            Console.WriteLine("Number of wheels: {0}", bike.NumberOfWheels);

            Console.WriteLine("\nMotorcycle");
            Console.WriteLine("Color: {0}{1}", mc.Color.Name, mc.Color.IsMetalic ? " metalic" : "");
            Console.WriteLine("Geras: {0} speed {1}", mc.Transmission.Geras, mc.Transmission.IsInternal ? "gearbox" : "external");
            Console.WriteLine("Number of wheels: {0}", mc.NumberOfWheels);
            Console.WriteLine("Power: {0} bhp", mc.HoresPower);

            Console.WriteLine("\nCar");
            Console.WriteLine("Color: {0}{1}", car.Color.Name, car.Color.IsMetalic ? " metalic" : "");
            Console.WriteLine("Geras: {0} speed {1}", car.Transmission.Geras, car.Transmission.IsInternal ? "gearbox" : "external");
            Console.WriteLine("Number of wheels: {0}", car.NumberOfWheels);
            Console.WriteLine("Power: {0} bhp", car.HorsePower);
        }

        private void Circle()
        {
            int[] diametersArray = { 23, 345, 576 };
            var circleList = new List<Circle>();
            Circle tempCircle;

            foreach (var i in diametersArray)
            {
                tempCircle = new Circle();
                tempCircle.SetDiameter(i);
                tempCircle.SetCenter = new Point((i + 10 - 9) * 22, (i + 5) * 22 - 500);
                circleList.Add(tempCircle);
            }

            Console.WriteLine("Circumferences and cordinats:");

            foreach (var c in circleList)
            {
                Console.WriteLine($"Circumferences: {c.GetCircumference():N2} (x: {c.GetCenter.GetX()}, y: {c.GetCenter.GetY()})");
            }
        }

        private void People()
        {
            string[] names = { "Erik", "Merethe", "Jörn", "Melissa", "Jon" };
            var personsList = new List<Person>();

            Person tempPerson;

            foreach (var s in names)
            {
                tempPerson = new Person();
                tempPerson.SetName(s);
                personsList.Add(tempPerson);
            }

            Console.WriteLine("Personer:");

            foreach (var p in personsList)
            {
                Console.WriteLine(p.GetName());
            }
        }

        private void ConsoleSeparator()
        {
            Console.WriteLine();
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine();
        }
    }
}
