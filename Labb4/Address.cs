﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb4
{
    class Address
    {
        private string _street;
        private int _zip;
        private string _city;

        internal string Street
        {
            get
            {
                return _street;
            }

            set
            {
                _street = value;
            }
        }

        internal int Zip
        {
            get
            {
                return _zip;
            }

            set
            {
                _zip = value;
            }
        }

        internal string City
        {
            get
            {
                return _city;
            }

            set
            {
                _city = value;
            }
        }

        internal Address(string street, int zip, string city)
        {
            this.Street = street;
            this.Zip = zip;
            this.City = city;
        }

        public override string ToString()
        {
            return $"{this.Street}, {this.Zip} {this.City}"; 
        }
    }
}
