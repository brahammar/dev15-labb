﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb4
{
    class Bike
    {
        private int _gears;
        private int _activeGear;
        private string _type;
        private List<string> _validTypes = new List<string> { "Road", "Mounten", "City", "Hybrid" };

        internal string Brand { get; private set; }
        internal string Color { get; set; }
        internal Wheel FrontWheel { get; set; }
        internal Wheel BackWheel { get; set; }
        internal string Type
        {
            get
            {
                return this._type;
            }
            private set
            {
                if (this._validTypes.Contains(value.ToLower(), StringComparer.OrdinalIgnoreCase))
                    this._type = value;
                else
                    throw new Exception("Type is invalid.");
            }
        }
        internal int Gears
        {
            get
            {
                return this._gears;
            }
            set
            {
                if (value > 0 && value < 30)
                    this._gears = value;
                else
                    throw new Exception("Number of gears out of range.");
            }
        }
        internal int ActiveGear
        {
            get
            {
                return this._activeGear;
            }
            set
            {
                if (value > this.Gears || value < 1)
                    throw new Exception("Gear change is out of bounds.");

                this._activeGear = value;
            }
        }

        internal static int NumberOfBikes { get; private set; } = 0;
        internal Bike()
        {
            NumberOfBikes++;
        }
        internal Bike(string brand) : this()
        {
            this.Brand = brand;
        }
        internal Bike(string brand, string color) : this(brand)
        {
            this.Color = color;
        }

        internal Bike(string brand, string color, string type, int gears, int activeGear, Wheel backWheel, Wheel frontWheel) : this(brand, color)
        {
            this.Type = type;
            this.Gears = gears;
            this.ActiveGear = activeGear;
            this.BackWheel = backWheel;
            this.FrontWheel = frontWheel;
        }

        // TODO: Kolla hur man hanterar den här typen av konstruktorer.
        internal Bike(string brand, string color, string type, int gears, int activeGear, int wheelDiameter, double wheelPresure, int wheelWidth, int wheelSpokes) :
            this(brand, color, type, gears, activeGear, new Wheel(wheelDiameter, wheelPresure, wheelWidth, wheelSpokes), new Wheel(wheelDiameter, wheelPresure, wheelWidth, wheelSpokes))
        { }

        internal bool ChangeGear(int change)
        {
            try
            {
                this.ActiveGear += change;
            }
            catch
            {
                return false;
            }

            return true;
        }

        internal void RingBell()
        {
            Console.Beep(5000, 5000);
        }

        public override string ToString()
        {
            return $"Brand: {this.Brand}, Color: {this.Color}, Type: {this.Type}, Number of gears: {this.Gears}, Active gear: {this.ActiveGear}, Front wheel: {this.FrontWheel.ToString()}, Back wheel: {this.BackWheel.ToString()} - {NumberOfBikes}";
        }
    }
}
