﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb4
{
    public class Car
    {
        private string _type;

        public string Color { get; set; }
        public int NumberOfGears { get; set; }
        public string Type { get
            {
                return this._type;
            }
            set
            {
                if (value.ToLower() == "kombi" || value.ToLower() == "kupe")
                    this._type = value;
            }
        }

        public Car(string color, int numberOfGears, string type)
        {
            this.Color = color;
            this.NumberOfGears = numberOfGears;
            this.Type = type;
        }

        public override string ToString()
        {
            return $"Color: {Color}, Gears: {NumberOfGears}, Type {Type}";
        }
    }
}



