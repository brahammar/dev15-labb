﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb4
{
    class House
    {
        private int _area;
        private int _rooms;
        private int _entrences;
        private int _windows;
        private int _floors;
        private int _bathRooms;
        private bool _hasAlarm;
        private bool _alarmIsActive;
        private int _openEntrances;

        internal string Name { get; private set; }

        internal Address Address { get; private set; }

        internal int Area
        {
            get
            {
                return _area;
            }

            set
            {
                _area = value;
            }
        }

        internal int Rooms
        {
            get
            {
                return _rooms;
            }

            set
            {
                _rooms = value;
            }
        }

        internal int Entrences
        {
            get
            {
                return _entrences;
            }

            set
            {
                _entrences = value;
            }
        }

        internal int Windows
        {
            get
            {
                return _windows;
            }

            set
            {
                _windows = value;
            }
        }

        internal int Floors
        {
            get
            {
                return _floors;
            }

            set
            {
                _floors = value;
            }
        }

        internal int BathRooms
        {
            get
            {
                return _bathRooms;
            }

            set
            {
                _bathRooms = value;
            }
        }

        internal bool HasAlarm
        {
            get
            {
                return _hasAlarm;
            }

            set
            {
                _hasAlarm = value;
            }
        }

        internal bool AlarmIsActive
        {
            get
            {
                if (!this.HasAlarm)
                    throw new Exception("House dose not have alarm.");
                else
                    return _alarmIsActive;
            }

            set
            {
                if (_alarmIsActive == value)
                    throw new Exception(string.Format("Alarm is already {0}.", value ? "armed" : "disarmed"));

                _alarmIsActive = value;
            }
        }

        public int OpenEntrances
        {
            get
            {
                return _openEntrances;
            }

            private set
            {
                if (this.Entrences >= value && value > 0)
                    _openEntrances = value;
                else
                    throw new Exception(string.Format("All entrences are {0}.", value == 0 ? "closed" : "open"));
            }
        }

        internal House(string name)
        {
            this.Name = name;
            //this.AlarmIsActive = false;
            //this.OpenEntrances = 0;
        }

        internal House(string name, int area, int rooms, int entrences, int windows, int floors, int bathRooms, bool hasAlarm) : this(name)
        {
            this.Area = area;
            this.Rooms = rooms;
            this.Entrences = entrences;
            this.Windows = windows;
            this.Floors = floors;
            this.BathRooms = bathRooms;
            this.HasAlarm = HasAlarm;
        }

        internal House(string name, Address address, int area, int rooms, int entrences, int windows, int floors, int bathRooms, bool hasAlarm) : this(name, area, rooms, entrences, windows, floors, bathRooms, hasAlarm)
        {
            this.Address = address;
        }

        internal Address SetAddress(string street, int zip, string city)
        {
            this.Address = new Address(street, zip, city);

            return this.Address;
        }

        internal bool AlarmSet()
        {
            try
            {
                this.AlarmIsActive = true;
                return true;
            }
            catch (Exception ex)
            {
                new Logger(ex.Message);
            }

            return false;
        }

        internal bool AlarmDisarm()
        {
            try
            {
                this.AlarmIsActive = false;
                return true;
            }
            catch (Exception ex)
            {
                new Logger(ex.Message);
            }

            return false;
        }

        internal bool OpenEntrance()
        {
            try
            {
                this.OpenEntrances++;
                return true;
            }
            catch (Exception ex)
            {
                new Logger(ex.Message);
            }

            return false;
        }

        internal bool CloseEntrance()
        {
            try
            {
                this.OpenEntrances--;
                return true;
            }
            catch (Exception ex)
            {
                new Logger(ex.Message);
            }

            return false;
        }

        public override string ToString()
        {
            return $"{Name}, {Address.ToString()}, has {Rooms} rooms with a total area of {Area}. The exterior of the house have {Entrences} entrances and {Windows} windows."; 
        }
    }
}
