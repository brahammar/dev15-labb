﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb4
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Car();

                Tuner();

                //MyBikes();

                //Houses();

            }
            catch (Exception ex)
            {
                new Logger(ex.ToString());
                new Logger("\n\r\n\r" + ex.Message);
            }

            Console.ReadKey();
        }

        private static void Houses()
        {
            House house1 = new House("Eriks hus", 140, 5, 2, 16, 2, 2, true);
            house1.SetAddress("Andresgaten 11A", 21567, "Malmö");

            house1.OpenEntrance();
            house1.OpenEntrance();
            house1.OpenEntrance();

            Console.WriteLine("Larma: " + house1.AlarmSet());
            Console.WriteLine("Larma: " + house1.AlarmSet());

            Console.WriteLine(house1.ToString());
        }

        private static void MyBikes()
        {
            Bike b1 = new Bike("Brand1", "Red", "Road", 22, 8, 700, 8, 23, 32);
            Console.WriteLine(b1);
            Bike b2 = new Bike("Brand2", "Blue", "City", 1, 1, 700, 5, 28, 32);
            Console.WriteLine(b2);
            Bike b3 = new Bike("Brand3", "Green", "Mounten", 21, 2, 700, 4, 48, 36);
            Console.WriteLine(b3);
            Bike b4 = new Bike("Brand4", "Pink", "Hybrid", 7, 3, 700, 6, 30, 32);
            Console.WriteLine(b4);

            Console.WriteLine("------------------");

            b4.ToString();
            Console.WriteLine($"Active gear: {b4.ActiveGear}.");
            b4.ChangeGear(-4);
            Console.WriteLine($"Active gear: {b4.ActiveGear}.");

            b4.RingBell();
        }

        private static void Tuner()
        {
            Random random = new Random();

            Radio radio;

            for (int i = 0; i < 10; i++)
            {
                System.Threading.Thread.Sleep(33);
                radio = new Radio() { Volume = random.Next(0, 100), Frequency = random.Next(875, 1080) / 10d };

                Console.WriteLine(radio.ToString());
            }
        }

        private static void Car()
        {
            Car car = new Car("Red", 5, "Kombi");
            Console.WriteLine($"Kombi: {car.Type}.");
            car.Type = "SUV";
            Console.WriteLine($"SUV: {car.Type}.");
        }
    }
}