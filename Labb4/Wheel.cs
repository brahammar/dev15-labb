﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb4
{
    class Wheel
    {
        internal int Diameter { get; private set; }
        internal double Presure { get; set; }
        internal int Width { get; set; }
        internal int Spooks { get; private set; }

        internal Wheel(int diameter, double presure, int width, int spooks)
        {
            this.Diameter = diameter;
            this.Presure = presure;
            this.Width = width;
            this.Spooks = spooks;
        }

        public override string ToString()
        {
            return $"Diameter: {this.Diameter}, Presure: {this.Presure}, Width: {this.Width}, Spooks: {this.Spooks}";
        }
    }
}
