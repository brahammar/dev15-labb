﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Labb4;

namespace Labb6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Literals();   // Uppgigt 1
            BitWiseTest();   // Uppgift 2
            //Pointing();   // Uppgift 3
            //DoubleTrubbel();   // Uppgift 4
            //CarsTheMovie();   // Uppgift 5
        }

        private static void CarsTheMovie()
        {
            Car car = new Car("Red", 5, "Kombi");
            Console.WriteLine($"Car:{car}");

            SUV suv = (SUV)car;
            Console.WriteLine($"SUV: {suv}");

            SUV suv2 = new SUV("Blue", 6);
            Console.WriteLine($"SUV2: {suv2}");
        }

        private static void DoubleTrubbel()
        {
            MyDoubleType mdt1 = new MyDoubleType(5);
            Console.WriteLine("mdt1: " + mdt1);

            MyDoubleType mdt2 = mdt1 + 5;
            Console.WriteLine("mdt1 + 5: " + mdt2);

            MyDoubleType mdt3 = mdt1 + mdt2;
            Console.WriteLine("mdt1 + mdt2: " + mdt3);

            MyDoubleType mdt4 = new MyDoubleType(5);
            Console.WriteLine("new MyDoubleType(5): " + mdt4);
            bool b = mdt1 == mdt4;
            Console.WriteLine("mdt1: " + mdt1);
            Console.WriteLine("mdt2: " + mdt2);
            Console.WriteLine("mdt3: " + mdt3);
            Console.WriteLine("mdt4: " + mdt4);

            Console.WriteLine("mdt1: {0}, mdt2: {1}, mdt3: {2}, mdt1 > mdt2: {3}, mdt1 == mdt4: {4}",

                 mdt1, mdt2, mdt3, mdt1 > mdt2, mdt1 == mdt4);
        }

        private static void Pointing()
        {
            Point point = new Point() { X = 10, Y = 20 };
            Console.WriteLine($"Point: {point}");

            Point3D point3D = point;
            Console.WriteLine($"Point3D from Point: {point3D}");

            point3D.Z = 30;
            Console.WriteLine($"Point3D with Z: {point3D}");

            point = (Point)point3D;
            Console.WriteLine($"Point from Point3D: {point}");

            point += point;
            Console.WriteLine($"Point += {point.ToString()}");

            point += point3D;
            Console.WriteLine($"Point + Point3D {point.ToString()}");
        }

        private static void BitWiseTest()
        {
            var numberOne = 7;     
            var numberTwo = 13;

            #region AND (&)
            var result = numberOne & numberTwo;
            Console.WriteLine($"{GetIntBinaryString(numberOne)} - numnerOne: {numberOne}");
            Console.WriteLine($"{GetIntBinaryString(numberTwo)} - numnerTwo: {numberTwo}");
            Console.WriteLine($"{GetIntBinaryString(result)} - AND (&): {result} = {numberOne} & {numberTwo}");
            Console.WriteLine("---");
            #endregion

            #region OR (|)
            result = numberOne | numberTwo;
            Console.WriteLine($"{GetIntBinaryString(numberOne)} - numnerOne: {numberOne}");
            Console.WriteLine($"{GetIntBinaryString(numberTwo)} - numnerTwo: {numberTwo}");
            Console.WriteLine($"{GetIntBinaryString(result)} - OR (|): {result} = {numberOne} | {numberTwo}");
            Console.WriteLine("---");
            #endregion

            #region XOR (^)
            result = numberOne ^ numberTwo;
            Console.WriteLine($"{GetIntBinaryString(numberOne)} - numnerOne: {numberOne}");
            Console.WriteLine($"{GetIntBinaryString(numberTwo)} - numnerTwo: {numberTwo}");
            Console.WriteLine($"{GetIntBinaryString(result)} - XOR (^): {result} = {numberOne} ^ {numberTwo}");
            Console.WriteLine("---");
            #endregion

            #region Left SHIFT (<<)
            result = numberOne << numberTwo;
            Console.WriteLine($"{GetIntBinaryString(numberOne)} - numnerOne: {numberOne}");
            Console.WriteLine($"{GetIntBinaryString(numberTwo)} - numnerTwo: {numberTwo}");
            Console.WriteLine($"{GetIntBinaryString(result)} - Shift (<<): {result} = {numberOne} << {numberTwo}");
            Console.WriteLine("---");
            #endregion

            #region Right SHIFT (>>)
            result = numberOne >> numberTwo;
            Console.WriteLine($"{GetIntBinaryString(numberOne)} - numnerOne: {numberOne}");
            Console.WriteLine($"{GetIntBinaryString(numberTwo)} - numnerTwo: {numberTwo}");
            Console.WriteLine($"{GetIntBinaryString(result)} - Shift (>>): {result} = {numberOne} >> {numberTwo}");
            Console.WriteLine("---");
            #endregion

            #region NOT (~) numberOne
            result = ~numberOne;
            Console.WriteLine($"{GetIntBinaryString(numberOne)} - numnerOne: {numberOne}");
            Console.WriteLine($"{GetIntBinaryString(result)} - Not (~): {result} = ~{numberOne}");
            Console.WriteLine("---");
            #endregion

            #region NOT (~) numberTwo
            result = ~numberTwo;
            Console.WriteLine($"{GetIntBinaryString(numberTwo)} - numnerTwo: {numberTwo}");
            Console.WriteLine($"{GetIntBinaryString(result)} - Not (~): {result} = ~{numberTwo}");
            Console.WriteLine("---");
            #endregion
        }

        #region From http://www.dotnetperls.com/shift
        static string GetIntBinaryString(int n)
        {
            char[] b = new char[32];
            int pos = 31;
            int i = 0;

            while (i < 32)
            {
                if ((n & (1 << i)) != 0)
                {
                    b[pos] = '1';
                }
                else
                {
                    b[pos] = '0';
                }
                pos--;
                i++;
            }
            return new string(b);
        }

        #endregion
        private static void Literals()
        {
            //float floatNumber = 1.1F;
            //long longNummer = 123L;
            //ulong ulongNumber = 234UL;
            //char charChar = 'a';
            //string stringString = "a";
        }
    }
}
