﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb7
{
    internal class Album
    {
        internal List<Song> Songs { get; private set; }
        internal Genre Genre { get; set; }

        internal bool AddSong(Song song)
        {
            throw new NotImplementedException();
        }

        internal bool RemoveSong(Song song)
        {
            throw new NotImplementedException();
        }

        public override string ToString() => $"Genre: {Genre} [{Songs}]";
    }
}
