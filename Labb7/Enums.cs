﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb7
{
    internal enum MainMenuOptions
    {
        Instrument =1,
        Genre,
        Exit = 0
    }
    internal enum Genre
    {
        Pop = 1,
        Rock,
        Clasic,
        Punk,
        Exit =0
    }

    internal enum Instrumenttype
    {
        Vocal = 1,
        Guitar,
        Drums,
        Bass,
        Piano,
        Exit = 0
    }
}
