﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb7
{
    internal class Member : Person
    {
        internal Instrumenttype Instrumenttype { get; set; }

        public override string ToString() => $"{base.ToString()}, Instrument: {Instrumenttype}";

    }
}
