﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb7
{
    internal class MusicLibrary // : IDisposable    // TODO 5: Implementera Idisposable
    {
        internal List<Artist> Artists { get; private set; }

        internal MusicLibrary()
        {
            this.Artists = DataController.GetArtists();
        }

        internal List<Artist> GetAllArtistsByGenre(Genre genre)
        {
            throw new NotImplementedException();
        }

        internal bool AddArtist(Artist artist)
        {
            throw new NotImplementedException();
        }

        internal bool RemoveArtist(Artist artist)
        {
            throw new NotImplementedException();
        }



        //public void Dispose()
        //{
        //    DataController.SaveArtists(this.Artists);

        //    throw new NotImplementedException();
        //}
    }
}
