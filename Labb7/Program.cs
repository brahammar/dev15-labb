﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Labb7
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Program program = new Program();

            //program.MainMenu();
        }

        private void MainMenu()
        {
            bool exitProgram = false;
            while (!exitProgram)
            {
                PrintMenu(typeof(MainMenuOptions));
                var i = GetUserMenuSelection(typeof(MainMenuOptions));

                var menuOptions = (MainMenuOptions)i;
                var selection = "";

                switch (menuOptions)
                {
                    case MainMenuOptions.Instrument:
                        selection = SelectInstrument();
                        break;
                    case MainMenuOptions.Genre:
                        selection = SelectGenere();
                        break;
                    case MainMenuOptions.Exit:
                        exitProgram = true;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                Console.WriteLine(selection + " is added.");
            }
        }

        private string SelectInstrument()
        {
            PrintMenu(typeof(Instrumenttype));
            int i = GetUserMenuSelection(typeof(Instrumenttype));

            return Enum.GetValues(typeof(Instrumenttype)).Cast<object>().SingleOrDefault(item => (int)item == i).ToString();


        }

        private string SelectGenere()
        {
            PrintMenu(typeof(Genre));
            int i = GetUserMenuSelection(typeof(Genre));

            return Enum.GetValues(typeof(Genre)).Cast<object>().SingleOrDefault(item => (int)item == i).ToString();
        }

        private int GetUserMenuSelection(Type enumType)
        {
            int userInput;
            var values = Enum.GetValues(enumType).Cast<object>();

            Console.Write("Select option : ");
            while (!int.TryParse(Console.ReadKey().KeyChar.ToString(), out userInput) || !values.Any(item => (int)item == userInput))
            {
                Console.Write($"{userInput} is not valid. Pleas try agen : ");
            }

            return userInput;
        }

        private void PrintMenu(Type typeOfEnum)
        {
            foreach (var item in Enum.GetValues(typeOfEnum))
            {
                Console.WriteLine((int)item + ". " + item);
            }
        }
    }
}
