﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb7
{
    internal class Soloartist: Artist
    {
        internal Member Member { get; set; }

        public override string ToString() => $"{base.ToString()}, Member: {Member}";
    }
}
