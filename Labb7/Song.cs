﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb7
{
    internal struct Song
    {
        internal string Name { get; set; }
        public int Duration { get; set; }

        internal void AddDuration(int houres, int minutes, int secounds)
        {
            Duration = houres * 3600 + minutes * 60 + secounds;
        }

        private string _ConvertSecundsToString(int timeSecounds)
        {
            int houres = timeSecounds / 3600;
            timeSecounds %= 3600;
            int minutes = timeSecounds / 60;
            timeSecounds %= 60;
            int secounds = timeSecounds;

            return
                $"{(houres > 0 ? houres + "h " : "")}" +
                $"{(minutes > 0 ? minutes + "min " : "")}" +
                $"{(secounds > 0 ? secounds + "sec" : "")}";
        }

        public override string ToString() => $"{Name} : {_ConvertSecundsToString(Duration)}";
    }
}
